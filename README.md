# Mergeling

## Features

- Interactive web interface
- Supports stacking by 3s, 5s, or exactly.
- Can select which merge chain you care about, and it'll auto select the start and end level and show pretty pictures.
- You can add how many you have of each level and that'll be considered in the calculation.
- Tooltips that should help to explain the interface and how things are calculated.
- Calculations are made live as configuration is changed.
- Ability to disable the target and determine what you can create with your current inventory.
- A progress indicator showing how close you are to reaching your target.
- Tooltips on images showing information about that object.
- Ability to share your configuration
- Multiple calculators can be worked on in parallel with tabs. These tabs can be dragged to be sorted, and renamed by clicking the title in the calculator.
- Auto-save current settings
- and more...
