const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");
const WebpackBar = require("webpackbar");
const fs = require("fs");

// Don't open the browser during development
process.env.BROWSER = "none";

if ("CI" in process.env) {
  module.exports = {
    webpack: {
      plugins: []
    },
    plugins: []
  };
} else {
  module.exports = {
    devServer: {
      https: {
        key: fs.readFileSync("cert/app.test/key.pem"),
        cert: fs.readFileSync("cert/app.test/cert.pem")
      }
    },
    webpack: {
      plugins: [
        new WebpackBar({
          profile: true
        }),
        ...(process.env.NODE_ENV === "production"
          ? [
              new BundleAnalyzerPlugin({
                openAnalyzer: true,
                analyzerHost: "0.0.0.0"
              })
            ]
          : [])
      ]
    },
    plugins: []
  };
}
