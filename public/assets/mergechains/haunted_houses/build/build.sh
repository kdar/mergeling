#!/usr/bin/env bash

magick convert -background none \( -rotate 0 -flop -page +48-70 "Tombstone 6.png" \) \
 \( -page -80+30 -resize 80% "Tombstone 1.png" \) \
 \( -set page +40+50 "Tombstone 1.png" \) \
 \( -set page -220+25 -flop -resize 80% "Tombstone 3.png" \) \
 -layers merge -modulate 80,50,-120 ../0.png

magick convert Haunted_House_01.png -modulate 80,110,-70 ../1.png
magick convert Haunted_House_02.png -modulate 80,110,-70 ../2.png
magick convert Haunted_House_03.png -modulate 80,110,-40 ../3.png
magick convert Haunted_House_04.png -flop -modulate 100,100,40 ../4.png
magick convert Haunted_House_05.png -modulate 80,110,-40 ../5.png
magick convert Haunted_House_06.png -modulate 90,110,25 ../6.png
magick convert Haunted_House_07.png -modulate 90,80,-15 ../7.png
magick convert Haunted_House_08.png -modulate 80,110,155 ../8.png
# magick convert Haunted_House_09.png -modulate 80,110,-20 ../9.png
magick convert Haunted_House_10.png -modulate 100,110,-110 ../10.png

# for blend in $(identify -list compose|grep -v Blur ); do 
#   convert -label "$blend" Haunted_House_09.png color_overlay1.png -compose $blend -composite miff:-
# done | montage - -tile 5x result.png