#!/usr/bin/env bash

# ./color2alpha -ca "#ff0000" -cr "#00000000" -g 1 egg-mask.png egg-mask-2.png

# Put glow on level 0 egg
# magick convert 0-base.png -compose screen \( egg-mask.png -scale 185%x185% -gravity center \) -composite 0.png

# Put wings on level 1 dragon
magick convert -background none \( -rotate 0 -page +48+40 1-wing.png \) \( -rotate 20 -page +62+50 1-wing.png \) -page +0+0 1-dragon.png -layers merge ../1.png

# Put wings on level 2 dragon
magick convert -background none \( -rotate 0 -page +48+40 2-wing.png \) \( -rotate 20 -page +62+50 2-wing.png \) -page +0+0 2-dragon.png -layers merge ../2.png

# Put wings on level 3 dragon
magick convert -background none \( -rotate 0 -page +60+30 3-wing.png \) \( -rotate 20 -page +111+45 3-wing.png \) -page +0+0 3-dragon.png -layers merge ../3.png

# Put wings on level 4 dragon
magick convert -background none \( -rotate 0 -page +48+40 4-wing.png \)  -page +0+0 4-dragon.png \( -rotate 10 -set page +80+25 4-wing.png \) -layers merge ../4.png

# Generate level 5 nest
magick convert -background none \( "Nest 1 - Top.png" -set page +30-10  \) \
  \( -resize 30% -rotate 00 -set page +65-25 "../6.png" \) \
  \( -resize 30% -rotate 30 -set page +80-20 "../6.png" \) \
  \( -resize 30% -rotate -20 -set page +25-20 "../6.png" \) \
  \( "Nest 2 - Bottom.png" \) \
  -layers merge ../5.png

# Hue shift level 7 dragon
magick convert ../1.png -modulate 110,110,0 ../7.png

# Hue shift level 8 dragon
magick convert ../2.png -modulate 110,110,0 ../8.png

# Hue shift level 9 dragon
magick convert ../3.png -modulate 110,110,0 ../9.png

# Hue shift level 10 dragon
magick convert ../4.png -modulate 110,110,0 ../10.png