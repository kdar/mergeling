# Changelog

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 1.0.0

> 2019-05-24

### Features

- Multiple calculator support with tabs. These tabs can be dragged to be sorted, and renamed by clicking the title in the calculator.
- Refreshed site design with better font readability, colors, and spacing
- Add images to select dropdown and tabs

### Bug Fixes

- Fix living stones level 3 & 4 image being swapped
- Fix treasure chests level 1 & 2 image being swapped
- Fix select input not extending full width
- Fix search input not showing when merchain is null

### BREAKING CHANGES

The storage and share format has changed, so it won't be able to load your previous saved states. This will not happen again going forward.
