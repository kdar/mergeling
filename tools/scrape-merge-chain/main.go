package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"runtime"
	"strconv"
	"strings"
	"sync"

	"github.com/PuerkitoBio/goquery"
	"github.com/gocolly/colly"
	"github.com/gocolly/colly/queue"
	"github.com/iancoleman/orderedmap"
	"github.com/iancoleman/strcase"
)

// Map represents a key-value map common in merge data
type Map = map[string]interface{}

var wikiUrls = []string{
	"https://mergedragons.fandom.com/wiki/Dragon_Trees",
	"https://mergedragons.fandom.com/wiki/Fruit_Trees",
	"https://mergedragons.fandom.com/wiki/Grimm_Trees",
	"https://mergedragons.fandom.com/wiki/Midas_Trees",
	"https://mergedragons.fandom.com/wiki/Glowing_Dragon_Trees",

	"https://mergedragons.fandom.com/wiki/Magic_Currency",
	"https://mergedragons.fandom.com/wiki/Stone_Bricks",
	"https://mergedragons.fandom.com/wiki/Life_Orbs",

	"https://mergedragons.fandom.com/wiki/Life_Flowers",
	"https://mergedragons.fandom.com/wiki/Prism_Flowers",

	"https://mergedragons.fandom.com/wiki/Magic_Mushrooms",
	"https://mergedragons.fandom.com/wiki/Mushrooms",

	"https://mergedragons.fandom.com/wiki/Bushes",
	"https://mergedragons.fandom.com/wiki/Grass",
	"https://mergedragons.fandom.com/wiki/Graves",
	"https://mergedragons.fandom.com/wiki/Hills",
	"https://mergedragons.fandom.com/wiki/Living_Stones",
	"https://mergedragons.fandom.com/wiki/Water",

	"https://mergedragons.fandom.com/wiki/Bonus_Points",
	"https://mergedragons.fandom.com/wiki/Dragon_Homes",
	"https://mergedragons.fandom.com/wiki/Magic_Coin_Storage",
	"https://mergedragons.fandom.com/wiki/Treasure_Chests",
	"https://mergedragons.fandom.com/wiki/Zen_Temple",
	"https://mergedragons.fandom.com/wiki/Stone_Storage",

	"https://mergedragons.fandom.com/wiki/Forgotten_Flowers",

	"https://mergedragons.fandom.com/wiki/Haunted_Houses",
	"https://mergedragons.fandom.com/wiki/Mystic_Topiaries",
	"https://mergedragons.fandom.com/wiki/Mythical_Idols",
	"https://mergedragons.fandom.com/wiki/Monster_Idols",

	"https://mergedragons.fandom.com/wiki/Crimson_Dragons",
	"https://mergedragons.fandom.com/wiki/Gargoyle_Dragons",
	"https://mergedragons.fandom.com/wiki/Golem_Dragons",
	"https://mergedragons.fandom.com/wiki/Grass_Dragons",
	"https://mergedragons.fandom.com/wiki/Green_Dragons",
	"https://mergedragons.fandom.com/wiki/Rock_Dragons",
	"https://mergedragons.fandom.com/wiki/Rocs",
	"https://mergedragons.fandom.com/wiki/Sharp_Dragons",
	"https://mergedragons.fandom.com/wiki/Spotted_Dragons",
	"https://mergedragons.fandom.com/wiki/Toadstool_Dragons",
	"https://mergedragons.fandom.com/wiki/Nature_Dragons",
	"https://mergedragons.fandom.com/wiki/Sun_Dragons",
	"https://mergedragons.fandom.com/wiki/Butterfly_Dragons",
	"https://mergedragons.fandom.com/wiki/Cosmos_Dragons",
	"https://mergedragons.fandom.com/wiki/Friend_Dragons",
	"https://mergedragons.fandom.com/wiki/Life_Dragons",
	"https://mergedragons.fandom.com/wiki/Midas_Ducks",
	"https://mergedragons.fandom.com/wiki/Pegasus",
	"https://mergedragons.fandom.com/wiki/Prism_Dragons",
	"https://mergedragons.fandom.com/wiki/Tribal_Dragons",
	"https://mergedragons.fandom.com/wiki/Wise_Dragons",
	"https://mergedragons.fandom.com/wiki/Zen_Dragon",
	"https://mergedragons.fandom.com/wiki/Aqua_Dragons",
	"https://mergedragons.fandom.com/wiki/Basket_Dragons",
	"https://mergedragons.fandom.com/wiki/Bunny_Dragons",
	"https://mergedragons.fandom.com/wiki/Cat_Dragons",
	"https://mergedragons.fandom.com/wiki/Cool_Dragons",
	"https://mergedragons.fandom.com/wiki/Cupid_Dragons",
	"https://mergedragons.fandom.com/wiki/Deer_Dragons",
	"https://mergedragons.fandom.com/wiki/Diva_Dragons",
	"https://mergedragons.fandom.com/wiki/Dog_Dragons",
	"https://mergedragons.fandom.com/wiki/Doll_Dragons",
	"https://mergedragons.fandom.com/wiki/Fairy_Dragons",
	"https://mergedragons.fandom.com/wiki/Ghastly_Dragons",
	"https://mergedragons.fandom.com/wiki/Jester_Dragon",
	"https://mergedragons.fandom.com/wiki/Martian_Dragons",
	"https://mergedragons.fandom.com/wiki/Moon_Dragons",
	"https://mergedragons.fandom.com/wiki/Morpho_Dragons",
	"https://mergedragons.fandom.com/wiki/Onesie_Dragons",
	"https://mergedragons.fandom.com/wiki/Passion_Dragons",
	"https://mergedragons.fandom.com/wiki/Rising_Sun_Dragons",
	"https://mergedragons.fandom.com/wiki/Samba_Dragon",
	"https://mergedragons.fandom.com/wiki/Sea_Dragons",
	"https://mergedragons.fandom.com/wiki/Stadium_Dragons",
	"https://mergedragons.fandom.com/wiki/Turtle_Dragons",
	"https://mergedragons.fandom.com/wiki/Winter_Dragons",
	"https://mergedragons.fandom.com/wiki/Wood_Dragons",
	"https://mergedragons.fandom.com/wiki/Air_Dragons",
	"https://mergedragons.fandom.com/wiki/Earth_Dragons",
	"https://mergedragons.fandom.com/wiki/Flame_Dragons",
	"https://mergedragons.fandom.com/wiki/Sylvan_Dragons",
	"https://mergedragons.fandom.com/wiki/Water_Dragons",
	// "https://mergedragons.fandom.com/wiki/Ruby_Fire_Eggs",
	// "https://mergedragons.fandom.com/wiki/Sapphire_Eggs",
	// "https://mergedragons.fandom.com/wiki/Tanzanite_Eggs",
}

var (
	zenTempleDescriptions = map[string]string{
		"1":  "A gorgeous Zen Temple. Gives a Mystical Dragon Egg. Harvest for Topsoil every 5 mins. (Costs Gems to Upgrade.)",
		"2":  "A gorgeous Zen Temple. Gives a Mystical Dragon Egg. Harvest for Topsoil every 4 mins. (Costs Gems to Upgrade.)",
		"3":  "A gorgeous Zen Temple. Gives a Mystical Dragon Egg. Harvest for Topsoil every 2 mins. (Costs Gems to Upgrade.)",
		"4":  "A gorgeous Zen Temple. Gives a Mystical Dragon Whelp. Harvest for a Lvl 1 Hill every 5 mins. (Costs Gems to Upgrade.)",
		"5":  "A gorgeous Zen Temple. Gives a Mystical Dragon Whelp. Harvest for a Lvl 1 Hill every 4 mins. (Costs Gems to Upgrade.)",
		"6":  "A gorgeous Zen Temple. Gives a Mystical Dragon Whelp. Harvest for a Lvl 1 Hill every 3 mins. (Costs Gems to Upgrade.)",
		"7":  "A gorgeous Zen Temple. Gives 2x Mystical Dragon Whelps. Harvest for a Lvl 2 Hill every 5 mins. (Costs Gems to Upgrade.)",
		"8":  "A gorgeous Zen Temple. Gives a Mystical Dragon Whelp. Harvest for a Lvl 2 Hill every 4 mins. (Costs Gems to Upgrade.)",
		"9":  "A gorgeous Zen Temple. Gives a Mystical Dragon Whelp. Harvest for a Lvl 2 Hill every 3 mins. (Costs Gems to Upgrade.)",
		"10": "A gorgeous Zen Temple. Gives a Mystical Dragon Whelp. Harvest for a Lvl 3 Hill every 7 mins. (Costs Gems to Upgrade.)",
		"11": "A gorgeous Zen Temple. Gives 2x Mystical Dragon Whelp. Harvest for a Lvl 3 Hill every 5.5 mins. (Costs Gems to Upgrade.)",
		"12": "A gorgeous Zen Temple. Gives a Mystical Dragon Kid. Harvest for a Lvl 3 Hill every 4 mins. (Upgrade by Merging).",
		"13": "Wonder #14 of the Dragon World! Sometimes tap for a Mystical Dragon Egg. Harvest for a Lvl 4 Hill every 6 mins",
	}
)

func overrideZenTemple(j *orderedmap.OrderedMap) *orderedmap.OrderedMap {
	entries, _ := j.Get("entries")
	for _, v := range entries.([]Map) {
		if _, ok := v["description"]; !ok {
			v["description"] = zenTempleDescriptions[*v["level"].(*string)]
		}
	}

	return j
}

func overrides(j Map) Map {
	if _, ok := j["zen_temple"]; ok {
		j["zen_temple"] = overrideZenTemple(j["zen_temple"].(*orderedmap.OrderedMap))
	}

	return j
}

func getMergeChains(urls ...string) interface{} {
	j := make(Map)
	m := sync.Mutex{}
	c := colly.NewCollector()
	q, _ := queue.New(
		runtime.NumCPU(),
		&queue.InMemoryQueueStorage{MaxSize: 10000},
	)

	// Find and visit all links
	c.OnHTML("body", func(e *colly.HTMLElement) {
		fields := []string{}
		chainData := orderedmap.New()
		var entries []Map

		name := e.DOM.Find("h1.page-header__title").First().Text()
		minLevel := 9999999
		maxLevel := 0
		lastLevel := 0

		e.DOM.Find("table.article-table").First().Find("tr").Each(func(i int, s *goquery.Selection) {
			if i == 0 {
				s.Find("th").Each(func(i int, s *goquery.Selection) {
					field := strcase.ToLowerCamel(strings.TrimSpace(s.Text()))
					if field == "lv" {
						field = "level"
					}

					fields = append(fields, field)
				})
			} else {
				local := make(Map)
				s.Find("td").Each(func(i int, s *goquery.Selection) {
					v := strings.TrimSpace(s.Text())
					vLower := strings.ToLower(v)
					value := &v

					if i == 0 {
						if *value == "" {
							minLevel = 0
							v = "0"
							value = &v
						} else if vLower == "wonder" || vLower == "nest" || vLower == "egg" {
							local["type"] = vLower

							if lastLevel != 0 {
								lastLevel++
								if lastLevel > maxLevel {
									maxLevel = lastLevel
								}
							}

							v = strconv.Itoa(lastLevel)
							value = &v
						} else {
							num, err := strconv.Atoi(*value)
							if err != nil {
								log.Fatal(err)
							}
							if num < minLevel {
								minLevel = num
							}
							if num > maxLevel {
								maxLevel = num
							}

							lastLevel = num
						}
					}

					if strings.ToLower(*value) == "n/a" {
						value = nil
					}

					local[fields[i]] = value
				})
				entries = append(entries, local)
			}
		})

		e.DOM.Find(".categories li[data-name]").Each(func(i int, s *goquery.Selection) {
			if v, ok := s.Attr("data-name"); ok {
				if v == "Dragons" {
					chainData.Set("type", "dragon")
				}
			}
		})

		chainData.Set("name", name)
		chainData.Set("startLevel", minLevel)
		chainData.Set("endLevel", maxLevel)
		chainData.Set("entries", entries)

		order := map[string]int{
			"name":       0,
			"startLevel": 1,
			"endLevel":   2,
			"entries":    3,
		}
		chainData.Sort(func(a *orderedmap.Pair, b *orderedmap.Pair) bool {
			return order[a.Key()] < order[b.Key()]
		})

		m.Lock()
		defer m.Unlock()
		j[strcase.ToSnake(name)] = chainData
	})

	for _, url := range urls {
		if err := q.AddURL(url); err != nil {
			log.Fatal(err)
		}
	}

	if err := q.Run(c); err != nil {
		log.Fatal(err)
	}

	return overrides(j)
}

func main() {
	var j interface{}
	if len(os.Args) == 1 {
		j = getMergeChains(wikiUrls...)
	} else {
		j = getMergeChains(os.Args[1:]...)
	}

	b, err := json.Marshal(j)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("// This file was generated by scrape-merge-chain.")
	fmt.Println()
	fmt.Print("export default ")
	fmt.Print(string(b))
	fmt.Println(";")
}
