import mergeChains from "../src/merge-chains";

let fields = [];

let initial = true;
for (let key in mergeChains) {
  for (let k in mergeChains[key].entries) {
    let newFields = Object.keys(mergeChains[key].entries[k]);
    if (initial) {
      fields = newFields;
      initial = false;
    } else {    
      fields = fields.filter(value => newFields.includes(value));
    }
  }
}

console.log(fields);
