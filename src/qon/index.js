export function serialize(o) {
  switch (o) {
    case null:
      return "null";
    case undefined:
      return "null";
    case true:
      return "true";
    case false:
      return "false";
    default:
  }

  switch (o.constructor.name) {
    case "Array":
      const aresult = o.map(v => serialize(v));
      return `[${aresult.join(",")}]`;
    case "Object":
      if (Object.keys(o).length === 0) {
        return "{}";
      }

      const result = [];
      for (const key in o) {
        if (!o.hasOwnProperty(key)) {
          continue;
        }
        const value = o[key];

        result.push(`${key}=${serialize(value)}`);
      }

      return `{${result.join(",")}}`;
    case "String":
      return `"${encodeURIComponent(o)}"`;
    default:
      return o.toString();
  }
}

export function lexer(input) {
  let c = 0;
  let text = "";
  let tokens = [];

  const peek = x => input[c + (x || 0)];
  const consume = () => input[c++];
  const string = () => {
    let s = "";
    while (peek() !== '"') {
      s += consume();

      if (peek() === "\\" && peek(1) === '"') {
        s += '"';
        consume();
        consume();
      }
    }
    consume();
    return decodeURIComponent(s);
  };
  const parseValue = v => {
    if (v === "true") {
      return true;
    } else if (v === "false") {
      return false;
    } else if (v === "null") {
      return null;
    } else if (v === "undefined") {
      return null;
    } else {
      return Number(v);
    }
  };

  while (true) {
    const v = consume();
    if (v === undefined) break;

    switch (v) {
      case "{":
        tokens.push({
          type: "object_start"
        });
        break;
      case "}":
        if (text !== "") {
          tokens.push({
            type: "value",
            data: parseValue(text)
          });
          text = "";
        }
        tokens.push({
          type: "object_end"
        });
        break;
      case ",":
        if (text !== "") {
          tokens.push({
            type: "value",
            data: parseValue(text)
          });
          text = "";
        }
        // tokens.push({
        //   type: "comma"
        // });
        break;
      case " ":
        break;
      case "\t":
        break;
      case "\n":
        break;
      case "[":
        tokens.push({
          type: "array_start"
        });
        break;
      case "]":
        if (text !== "") {
          tokens.push({
            type: "value",
            data: parseValue(text)
          });
          text = "";
        }
        tokens.push({
          type: "array_end"
        });
        break;
      case "=":
        if (text !== "") {
          tokens.push({
            type: "key",
            name: text
          });
          text = "";
        }
        // tokens.push({
        //   type: "equal"
        // });
        break;
      case '"':
        tokens.push({
          type: "value",
          data: string()
        });
        break;
      default:
        text += v;
    }
  }

  if (text.length > 0) {
    tokens.push({
      type: "value",
      data: parseValue(text)
    });
  }

  return tokens;
}

export function deserialize(input) {
  if (!input) return null;

  let c = 0;
  let tokens = lexer(input);

  const isEmpty = () => tokens.length === 0 || tokens.length === c + 1;
  const peek = x => tokens[c + (x || 0)];
  const consume = () => tokens[c++];

  const parse_object = () => {
    let o = {};
    consume();

    while (!isEmpty()) {
      const t = peek();
      switch (t.type) {
        case "object_end":
          consume();
          return o;
        case "key":
          let [key, value] = parse_key_value();
          o[key] = value;
          break;
        default:
          throw new Error(`parse_object: unexpected token type: ${t.type}`);
      }
    }

    return o;
  };

  const parse_array = () => {
    let a = [];
    consume();

    while (!isEmpty()) {
      const t = peek();
      switch (t.type) {
        case "array_start":
          a.push(parse_array());
          break;
        case "array_end":
          consume();
          return a;
        case "object_start":
          a.push(parse_object());
          break;
        case "value":
          a.push(consume().data);
          break;
        default:
          throw new Error(`parse_array: unexpected token type: ${t.type}`);
      }
    }

    return a;
  };

  const parse_key_value = () => {
    let key = consume().name;
    let value = parse();
    return [key, value];
  };

  const parse = () => {
    const t = peek();
    switch (t.type) {
      case "object_start":
        return parse_object();
      case "array_start":
        return parse_array();
      case "value":
        return consume().data;
      default:
        throw new Error(`parse: unexpected token type: ${t.type}`);
    }
  };

  return parse();
}
