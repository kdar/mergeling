import { serialize, deserialize } from "./index.js";

const tests = [
  [
    "{a=1}",
    {
      a: 1
    }
  ],
  [
    `{a=1,b="hey"}`,
    {
      a: 1,
      b: "hey"
    }
  ],
  [
    '{d=[{k="hey",v=5}]}',
    {
      d: [
        {
          k: "hey",
          v: 5
        }
      ]
    }
  ],
  [
    `{a=0,b=1,c=0,d=[{k=null,v=null}],e="5",f=1,g=99,h={k=3,v=2}}`,
    {
      a: 0,
      b: 1,
      c: 0,
      d: [
        {
          k: null,
          v: null
        }
      ],
      e: "5",
      f: 1,
      g: 99,
      h: {
        k: 3,
        v: 2
      }
    }
  ],
  [
    `{d=[{k=1}],e=2}`,
    {
      d: [
        {
          k: 1
        }
      ],
      e: 2
    }
  ],
  [
    `{d=[{k={v=1}}],e=2}`,
    {
      d: [
        {
          k: {
            v: 1
          }
        }
      ],
      e: 2
    }
  ],
  [
    `{d=[5],e=2}`,
    {
      d: [5],
      e: 2
    }
  ],
  [
    `{d=[[[5]]],e=[[[{k=5}]]]}`,
    {
      d: [[[5]]],
      e: [
        [
          [
            {
              k: 5
            }
          ]
        ]
      ]
    }
  ],
  [`5`, 5],
  [`5.89`, 5.89],
  [`[5]`, [5]],
  [`[1,2,3,4,5]`, [1, 2, 3, 4, 5]],
  [`[]`, []],
  [`{}`, {}],
  [`null`, null],
  [`true`, true],
  [`false`, false],
  [
    `{a=0,b=1,c=0,d=[{k=1,v=2},{k=2,v=1},{k=3,v=1},{k=null,v=null}],e="5",f=1,g=99,h={k=4,v=2}}`,
    {
      a: 0,
      b: 1,
      c: 0,
      d: [
        {
          k: 1,
          v: 2
        },
        {
          k: 2,
          v: 1
        },
        {
          k: 3,
          v: 1
        },
        {
          k: null,
          v: null
        }
      ],
      e: "5",
      f: 1,
      g: 99,
      h: {
        k: 4,
        v: 2
      }
    }
  ],
  [
    `{a=[{k=1},{k=2}]}`,
    {
      a: [
        {
          k: 1
        },
        {
          k: 2
        }
      ]
    }
  ],
  [
    `{title="%22default%22"}`,
    {
      title: `"default"`
    }
  ]
];

const deserializeOnlyTests = [[`undefined`, null]];

describe("deserialize", () => {
  test.each(tests)("deserialize(%p) == %p", (input, output) => {
    expect(deserialize(input)).toEqual(output);
  });

  test.each(deserializeOnlyTests)("deserialize(%p) == %p", (input, output) => {
    expect(deserialize(input)).toEqual(output);
  });
});

describe("deserialize with spaces", () => {
  test.each(
    tests.map(v => [
      v[0]
        .replace("=", " = ")
        .replace("[", " [ ")
        .replace("]", " ] ")
        .replace(",", " , "),
      v[1]
    ])
  )("deserialize(%p) == %p", (input, output) => {
    expect(deserialize(input)).toEqual(output);
  });
});

describe("serialize", () => {
  test.each(tests)("%p == serialize(%p)", function(output, input) {
    expect(serialize(input)).toEqual(output);
  });
});
