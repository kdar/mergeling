// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT License was not distributed with this
// file, you can obtain one at https://opensource.org/licenses/MIT.
//
// Copyright (c) Kevin Darlington. All rights reserved.

import { calculate } from "./calc.js";

const commonInputs = [
  {
    entries: {},
    target: {
      key: 3,
      value: 2
    }
  },
  {
    entries: {
      3: 5
    },
    target: {
      key: 3,
      value: 2
    }
  },
  {
    entries: {
      2: 6
    },
    target: {
      key: 3,
      value: 2
    }
  },
  {
    entries: {
      1: 6,
      2: 5
    },
    target: {
      key: 3,
      value: 2
    }
  },
  {
    entries: {
      1: 6,
      2: 6
    },
    target: {
      key: 3,
      value: 2
    }
  },
  {
    entries: {
      1: 6,
      2: 6,
      3: 5
    },
    target: {
      key: 4,
      value: 3
    }
  }
];

const optimizeNoneExpected = [
  {
    amounts: {
      "1": 18,
      "2": 6
    },
    leftover: {
      "1": 0,
      "2": 0,
      "3": 2
    }
  },

  {
    amounts: {
      "1": 0,
      "2": 0
    },
    leftover: {
      "1": 0,
      "2": 0,
      "3": 5
    }
  },

  {
    amounts: {
      "1": 0,
      "2": 0
    },
    leftover: {
      "1": 0,
      "2": 0,
      "3": 2
    }
  },

  {
    amounts: {
      "1": 0,
      "2": 1
    },
    leftover: {
      "1": 3,
      "2": 0,
      "3": 2
    }
  },

  {
    amounts: {
      "1": 0,
      "2": 0
    },
    leftover: {
      "1": 6,
      "2": 0,
      "3": 2
    }
  },

  {
    amounts: {
      "1": 12,
      "2": 6,
      "3": 4
    },
    leftover: {
      "1": 0,
      "2": 0,
      "3": 0,
      "4": 3
    }
  }
];

const optimizeGreedyExpected = [
  {
    amounts: {
      "1": 15,
      "2": 6
    },
    leftover: {
      "1": 0,
      "2": 1,
      "3": 2
    }
  },
  {
    amounts: {
      "1": 0,
      "2": 0
    },
    leftover: {
      "1": 0,
      "2": 0,
      "3": 5
    }
  },
  {
    amounts: {
      "1": 0,
      "2": 0
    },
    leftover: {
      "1": 0,
      "2": 1,
      "3": 2
    }
  },
  {
    amounts: {
      "1": 0,
      "2": 0
    },
    leftover: {
      "1": 6,
      "2": 0,
      "3": 2
    }
  },
  {
    amounts: {
      "1": 0,
      "2": 0
    },
    leftover: {
      "1": 6,
      "2": 1,
      "3": 2
    }
  },
  {
    amounts: {
      "1": 19,
      "2": 10,
      "3": 6
    },
    leftover: {
      "1": 0,
      "2": 1,
      "3": 1,
      "4": 4
    }
  }
];

const optimizeExactExpected = [
  {
    amounts: {
      "1": 13,
      "2": 5
    },
    leftover: {
      "1": 0,
      "2": 0,
      "3": 2
    }
  },
  {
    amounts: {
      "1": 0,
      "2": 0
    },
    leftover: {
      "1": 0,
      "2": 0,
      "3": 5
    }
  },
  {
    amounts: {
      "1": 0,
      "2": 0
    },
    leftover: {
      "1": 0,
      "2": 1,
      "3": 2
    }
  },
  {
    amounts: {
      "1": 0,
      "2": 0
    },
    leftover: {
      "1": 6,
      "2": 0,
      "3": 2
    }
  },
  {
    amounts: {
      "1": 0,
      "2": 0
    },
    leftover: {
      "1": 6,
      "2": 1,
      "3": 2
    }
  },
  {
    amounts: {
      "1": 0,
      "2": 2,
      "3": 3
    },
    leftover: {
      "1": 1,
      "2": 0,
      "3": 0,
      "4": 3
    }
  }
];

const optimizeNoneCases = optimizeNoneExpected.map((expected, i) => [
  Object.assign({}, commonInputs[i], {
    optimize: "none"
  }),
  expected
]);

const optimizeGreedyCases = optimizeGreedyExpected.map((expected, i) => [
  Object.assign({}, commonInputs[i], {
    optimize: "greedy"
  }),
  expected
]);

const optimizeExactCases = optimizeExactExpected.map((expected, i) => [
  Object.assign({}, commonInputs[i], {
    optimize: "exact"
  }),
  expected
]);

test.each(optimizeNoneCases)(
  "[optimize: none] %#: calculate(%j) should return %j",
  (input, expected) => {
    let result = calculate(input);
    expect(result).toEqual(expected);
  }
);

test.each(optimizeGreedyCases)(
  "[optimize: greedy] %#: calculate(%j) should return %j",
  (input, expected) => {
    let result = calculate(input);
    expect(result).toEqual(expected);
  }
);

test.each(optimizeExactCases)(
  "[optimize: exact] %#: calculate(%j) should return %j",
  (input, expected) => {
    let result = calculate(input);
    expect(result).toEqual(expected);
  }
);
