import pako from "pako";

import { serialize, deserialize } from "../qon";

// Change this every time the format changes in a non-backwards compatible way.
// This version will specify the format in which it is encoded, and the data format
// itself. E.g. version 5 could signify the encoding as base64'ed brotli, and the
// data format being {"hey": "there"}, while version 6 could be the same encoding but
// the data format being {"h": "there"}, and version 7 could be base73'ed brotli and
// the data format being {"h": "there"}, etc...
const currentVersion = 2;
const versionSeparator = "!";

const pipelines = {
  2: {
    encode: [processState, serialize, encodev2],
    decode: [decodev2, deserialize],
    updateToNext: null
  }
};

function unescape(str) {
  return (str + "===".slice((str.length + 3) % 4))
    .replace(/-/g, "+")
    .replace(/_/g, "/");
}

function escape(str) {
  return str
    .replace(/\+/g, "-")
    .replace(/\//g, "_")
    .replace(/=/g, "");
}

function encodev2(str) {
  try {
    let buf = pako.deflateRaw(str, {
      level: 9,
      memLevel: 9,
      to: "string"
    });

    return `${currentVersion}${versionSeparator}${escape(btoa(buf))}`;
  } catch (e) {
    if (process.env.NODE_ENV === "development") {
      console.warn("encodev2", e);
    }

    return "ERROR";
  }
}

function decodev2(str) {
  try {
    let buf = atob(unescape(str));
    buf = pako.inflateRaw(buf, {
      to: "string"
    });
    return buf;
  } catch (e) {
    if (process.env.NODE_ENV === "development") {
      console.warn("decodev2", e);
    }
  }

  return null;
}

function processState(state) {
  // Delete the id for each calculator since we don't need it.
  // We don't really need to do this because when we load the
  // state we ignore any calculator ids, but it helps us save
  // some space when we encode it.
  const calculators = state.calculators.map(v => {
    const { id, ...rest } = v;
    return rest;
  });

  return {
    ...state,
    calculators: calculators
  };
}

export function encode(state) {
  let data = state;
  for (let x of pipelines[currentVersion].encode) {
    data = x(data);
  }

  return data;
}

export function decode(buf) {
  const versionSep = buf.indexOf(versionSeparator);
  if (versionSep < 0) return null;
  let version = parseInt(buf.slice(0, versionSep));
  if (isNaN(version)) return null;

  buf = buf.slice(versionSep + 1);

  let data = null;
  if (version in pipelines) {
    data = buf;

    for (let x of pipelines[version].decode) {
      data = x(data);
    }

    while (version in pipelines) {
      if (pipelines[version].updateToNext) {
        data = pipelines[version].updateToNext(data);
      }
      version += 1;
    }
  }

  return data;
}
