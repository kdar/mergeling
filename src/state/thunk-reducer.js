import { useReducer, useRef } from "react";

function useThunkReducer(reducer, initialState, init = undefined) {
  const [state, dispatch] = useReducer(reducer, initialState, init);
  const stateRef = useRef(null);
  stateRef.current = state;

  const getState = () => stateRef.current;
  const thunk = action => {
    if (typeof action === "function") {
      return action({ dispatch, getState });
    }
    return dispatch(action);
  };
  return [state, thunk];
}

export default useThunkReducer;
