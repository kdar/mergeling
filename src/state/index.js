import { useRef, useEffect, useCallback } from "react";
import useThunkReducer from "./thunk-reducer.js";
import queryString from "query-string";

import { encode, decode } from "./codecs.js";

export const defaultEndLevel = 99;
export const defaultTitle = "New";
const storageKey = "default";
let idNext = 1;

const debounce = (func, delay) => {
  let inDebounce;
  return function() {
    clearTimeout(inDebounce);
    inDebounce = setTimeout(() => func.apply(this, arguments), delay);
  };
};

const save = debounce((key, state) => {
  localStorage.setItem(key, encode(state));
}, 1000);

function initCalculatorState(state) {
  return Object.assign(
    {
      title: "New",
      mergeChain: null,
      enableTarget: true,
      entries: [{ k: null, v: null }],
      optimize: "5",
      startLevel: 1,
      endLevel: defaultEndLevel,
      target: { k: 3, v: 2 }
    },
    state || {},
    // Overwrite with our own id because the id the
    // calculator had previously does not matter. But
    // we need to do this here to avoid collisions.
    {
      id: idNext++
    }
  );
}

function initState(state) {
  const thestate = Object.assign(
    {
      tooltipExpand: true,
      calcIndex: 0,
      calculators: []
    },
    state || {}
  );

  if (Array.isArray(thestate.calculators) && thestate.calculators.length > 0) {
    thestate.calculators = thestate.calculators.map(v =>
      initCalculatorState(v)
    );
  } else {
    thestate.calculators = [initCalculatorState()];
  }

  return thestate;
}

function reducer(state, action) {
  switch (action.type) {
    case "set":
      return initState(action.data);
    case "toggle-tooltip-expand":
      return {
        ...state,
        tooltipExpand: !state.tooltipExpand
      };
    case "set-calc-index":
      return {
        ...state,
        calcIndex: action.index
      };
    case "reindex-calc":
      let calculators = state.calculators.slice();
      calculators.splice(
        action.newIndex < 0
          ? calculators.length + action.newIndex
          : action.newIndex,
        0,
        calculators.splice(action.oldIndex, 1)[0]
      );
      return {
        ...state,
        calcIndex: action.newIndex,
        calculators: calculators
      };
    case "delete-calc":
      let calcIndex = state.calcIndex;
      if (
        action.index < state.calcIndex ||
        calcIndex >= state.calculators.length - 1
      ) {
        calcIndex -= 1;
      }

      return {
        ...state,
        calcIndex: calcIndex,
        calculators: state.calculators.filter(
          (x, index) => index !== action.index
        )
      };
    case "add-calc":
      return {
        ...state,
        calcIndex: state.calculators.length,
        calculators: state.calculators.concat(initCalculatorState())
      };
    case "calc-set-entries":
      return {
        ...state,
        calculators: state.calculators.map((item, index) => {
          if (index !== action.calcIndex) {
            return item;
          }

          return {
            ...item,
            entries: action.data
          };
        })
      };
    case "calc-set":
      return {
        ...state,
        calculators: state.calculators.map((item, index) => {
          if (index !== action.calcIndex) {
            return item;
          }

          return action.data;
        })
      };
    case "calc-reset":
      return {
        ...state,
        calculators: state.calculators.map((item, index) => {
          if (index !== action.calcIndex) {
            return item;
          }

          return initCalculatorState();
        })
      };
    case "calc-rename":
      return {
        ...state,
        calculators: state.calculators.map((item, index) => {
          if (index !== action.calcIndex) {
            return item;
          }

          return {
            ...state.calculators[index],
            title: action.title
          };
        })
      };
    default:
      throw new Error(`unknown action ${action.type}`);
  }
}

function getStateFromHash() {
  let state = null;
  const parsedHash = queryString.parse(window.location.hash);
  if (parsedHash["state"]) {
    state = decode(parsedHash["state"]);
    window.history.replaceState({}, "", "/");
  }
  return state;
}

export function useRootReducer() {
  let defaultState = null;
  const isLoading = useRef(true);
  if (isLoading.current) {
    let hashState = getStateFromHash();
    if (hashState) {
      defaultState = hashState;
    } else {
      try {
        defaultState = decode(localStorage.getItem(storageKey));
      } catch (e) {
        if (process.env.NODE_ENV === "development") {
          console.warn(
            `Failed to load state '${storageKey}' from localStorage; using default state instead`
          );
        }
      }
    }

    isLoading.current = false;
  }

  const [state, dispatch] = useThunkReducer(reducer, defaultState, initState);
  save(storageKey, state);

  const handleHashChange = useCallback(() => {
    const hashState = getStateFromHash();
    if (hashState) {
      dispatch({ type: "set", data: hashState });
    }
  }, [dispatch]);

  useEffect(() => {
    window.addEventListener("hashchange", handleHashChange, false);

    return function cleanup() {
      window.removeEventListener("hashchange", handleHashChange, false);
    };
  }, [handleHashChange]);

  return [state, dispatch];
}
