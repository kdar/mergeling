import React from "react";
import ReactDOM from "react-dom";
import tippy from "tippy.js";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs/esm/index.js";
import { SortableContainer, SortableElement } from "react-sortable-hoc";

import Layout from "./components/layout.js";
import { Calculator } from "./components/calculator.js";
import { useRootReducer } from "./state/index.js";
import { StateContext } from "./state/context.js";
import mergeChains from "./merge-chains.js";

import "normalize.css";
import "milligram";
import "tippy.js/dist/tippy.css";
import "./style/index.css";

tippy.setDefaultProps({ theme: "mergeling" });

const SortableItem = SortableElement(props => (
  <div
    className={`react-tabs__tab ${
      props.selected ? "react-tabs__tab--selected" : ""
    }`}
    onMouseDown={e => {
      if (e.target.classList.contains("react-tabs__tab-close")) {
        return;
      }

      props.dispatch({ type: "set-calc-index", index: props.sortIndex });
    }}
    key={`tab-${props.calculator.id}`}
  >
    {props.calculator.mergeChain && (
      <div className="mergechain-icon">
        <img
          alt={props.calculator.title}
          src={`assets/mergechains/${props.calculator.mergeChain}/${mergeChains[props.calculator.mergeChain].preferredIcon}.png`}
        />
      </div>
    )}
    {props.calculator.title}
    {props.selected && (
      <div
        className="react-tabs__tab-close"
        onClick={() => {
          props.dispatch({ type: "delete-calc", index: props.sortIndex });
        }}
      />
    )}
  </div>
));

SortableItem.tabsRole = "Tab";

const SortableList = SortableContainer(props => (
  <div className="react-tabs__tab-list">
    <div className="react-tabs__tab-list-left">
      {props.items.map((value, index) => (
        <SortableItem
          role="tab"
          key={`sortableitem-${value.id}`}
          index={index}
          sortIndex={index}
          selected={index === props.calcIndex}
          calculator={value}
          dispatch={props.dispatch}
        />
      ))}
    </div>
    <button
      className="react-tabs-add button button-small button-pop"
      onClick={() => props.dispatch({ type: "add-calc" })}
    >
      <div className="react-tabs-add-icon" />
    </button>
  </div>
));

SortableList.tabsRole = "TabList";

function App(props) {
  const [state, dispatch] = useRootReducer();

  const calcDispatch = index => action => {
    action.calcIndex = index;
    dispatch(action);
  };

  const onSortEnd = ({ oldIndex, newIndex }) => {
    dispatch({
      type: "reindex-calc",
      oldIndex: oldIndex,
      newIndex: newIndex
    });
  };

  return (
    <Layout state={state}>
      <div className="react-tabs react-tabs-proxy">
        <SortableList
          axis="xy"
          role="tablist"
          items={state.calculators}
          onSortEnd={onSortEnd}
          getHelperDimensions={({ node }) => ({
            width: node.getBoundingClientRect().width,
            height: node.getBoundingClientRect().height
          })}
          calcIndex={state.calcIndex}
          dispatch={dispatch}
          distance={5}
          shouldCancelStart={e => {
            if (e.target.classList.contains("react-tabs__tab-close")) {
              return true;
            }

            return SortableList.defaultProps.shouldCancelStart.call(this, e);
          }}
        />
      </div>

      <Tabs selectedIndex={state.calcIndex || 0} onSelect={() => {}}>
        <TabList>
          {state.calculators.map((v, index) => (
            <Tab key={`hiddentab-${v.id}`}>{v.title}</Tab>
          ))}
        </TabList>

        {state.calculators.map((v, index) => (
          <TabPanel key={`tabpanel-${v.id}`}>
            <StateContext.Provider
              key={`statecontext-${v.id}`}
              value={calcDispatch(index)}
            >
              <Calculator
                key={`calculator-${v.id}`}
                state={state.calculators[index]}
                tooltipExpand={state.tooltipExpand}
              />
            </StateContext.Provider>
          </TabPanel>
        ))}
      </Tabs>
    </Layout>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
