import mergeChains from "./merge-chains-gen.js";

const preferredIcons = {
  bonus_points: 1,
  bushes: 1,
  dragon_homes: 1,
  dragon_trees: 1,
  fruit_trees: 1,
  glowing_dragon_trees: 1,
  grass: 1,
  graves: 1,
  grimm_trees: 1,
  hills: 1,
  life_flowers: 1,
  life_orbs: 0,
  living_stones: 1,
  magic_coin_storage: 1,
  magic_currency: 1,
  magic_mushrooms: 1,
  midas_trees: 1,
  mushrooms: 1,
  prism_flowers: 1,
  stone_bricks: 1,
  stone_storage: 1,
  treasure_chests: 1,
  water: 1,
  zen_temple: 1,
  haunted_houses: 1
};

// Synthetic dragon merge chains. This is a quickfix to allow people
// to calculate dragon merge chains since they're all the same.
// One day, I may put special support for dragon chains in the UI.
mergeChains["dragon_tier1"] = {
  name: "Dragon tier1",
  startLevel: 0,
  endLevel: 5,
  entries: [
    {
      level: "0",
      name: "Dragon Egg"
    },
    {
      level: "1",
      name: "Dragon level 1"
    },
    {
      level: "2",
      name: "Dragon level 2"
    },
    {
      level: "3",
      name: "Dragon level 3"
    },
    {
      level: "4",
      name: "Dragon level 4"
    },
    {
      dragonPower: null,
      level: "5",
      name: "Nest of Dragon Eggs"
    }
  ]
};

mergeChains["dragon_tier2"] = {
  name: "Dragon tier2",
  startLevel: 6,
  endLevel: 10,
  entries: [
    {
      level: "6",
      name: "Dragon Egg"
    },
    {
      level: "7",
      name: "Dragon level 7"
    },
    {
      level: "8",
      name: "Dragon level 8"
    },
    {
      level: "9",
      name: "Dragon level 9"
    },
    {
      level: "10",
      name: "Dragon level 10"
    }
  ]
};

for (let key in mergeChains) {
  if (
    preferredIcons[key] === undefined &&
    process.env.NODE_ENV === "development"
  ) {
    // console.warn(`Missing preferred icon for ${key}`);
  }

  mergeChains[key]["preferredIcon"] =
    preferredIcons[key] || mergeChains[key].startLevel;

  // Delete the first level 0 if there are two, for now.
  if (
    mergeChains[key].entries[0].level === "0" &&
    mergeChains[key].entries[1].level === "0"
  ) {
    mergeChains[key].entries.splice(0, 1);
  }

  // TEMP(kdar): Delete dragon chains for now
  if (mergeChains[key]["type"] === "dragon") {
    delete mergeChains[key];
  }
}

export default mergeChains;
