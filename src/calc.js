// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT License was not distributed with this
// file, you can obtain one at https://opensource.org/licenses/MIT.
//
// Copyright (c) Kevin Darlington. All rights reserved.

export function positiveNumber(v) {
  let c = parseInt(v);
  c = Number.isNaN(c) ? null : c;
  if (c !== null && c < 0) {
    c = 0;
  }

  return c;
}

export function calculateWithTarget(opts) {
  let target = opts.target;
  let entries = opts.entries;

  if (opts.startLevel >= target.k || opts.endLevel < target.k) {
    return {
      amounts: {},
      leftover: {},
      maxNeeded: {}
    };
  }

  // We do this separately because of the way the algorithm
  // works for calculating the amounts and leftover, will
  // not always calculate all the max needed amounts for
  // each level. The algorithm could probably be tweaked
  // so it could, but this is just easier.
  let maxNeeded = {};
  let nextMaxNeeded = target.v;
  for (let x = target.k - 1; x >= opts.startLevel; x--) {
    if (opts.optimize === "exact") {
      maxNeeded[x] = Math.ceil((nextMaxNeeded * 5) / 2);
      nextMaxNeeded = maxNeeded[x];
    } else if (opts.optimize === "5") {
      maxNeeded[x] = Math.ceil(nextMaxNeeded / 2) * 5;
      nextMaxNeeded = maxNeeded[x];
    } else {
      maxNeeded[x] = nextMaxNeeded * 3;
      nextMaxNeeded = maxNeeded[x];
    }
  }

  let amounts = {};
  let leftover = {};
  for (let x = opts.startLevel; x <= target.k; x++) {
    if (!(x in entries)) {
      entries[x] = 0;
    }
    amounts[x] = 0;
    leftover[x] = 0;
  }

  let nextCount = target.v - entries[target.k];
  let found = false;
  for (let x = target.k - 1; x >= opts.startLevel; x--) {
    if (nextCount < 0) {
      leftover[x + 1] = entries[x + 1];
      break;
    }

    if (found) {
      leftover[x] = entries[x];
      continue;
    }

    if (opts.optimize === "exact") {
      let amount = Math.ceil((nextCount * 5) / 2) - entries[x];
      if (amount <= 0) {
        leftover[x] -= amount;
        found = true;
        amount = 0;
      }

      amounts[x] = amount;
      leftover[x + 1] = nextCount - amounts[x + 1];
    } else if (opts.optimize === "5") {
      let amount = Math.ceil(nextCount / 2) * 5 - entries[x];
      if (amount <= 0) {
        leftover[x] -= amount;
        found = true;
        amount = 0;
      }

      let nextAmount = Math.floor((amount + entries[x]) / 5) * 2;
      // This situation can occur if we have a ton of entries[x], which
      // will then push the nextAmount further. But, given we can at max
      // be 1 over the nextCount because of stacks of 5 turning into 2,
      // we should fix the max nextAmount to only be at most nextCount+1.
      if (nextAmount > nextCount + 1) {
        nextAmount = nextCount + 1;
      }
      amounts[x] = amount;
      leftover[x + 1] = nextAmount - amounts[x + 1];
      amounts[x + 1] = nextAmount;
    } else {
      let amount = nextCount * 3 - entries[x];
      if (amount > 0) {
        amounts[x] = amount;
      } else {
        leftover[x] -= amount;
        found = true;
      }
      leftover[x + 1] = nextCount - amounts[x + 1]; // (amount + entries[x]) / 3 - amounts[x + 1];
    }

    nextCount = amounts[x];
  }

  delete amounts[target.k];

  if (leftover[target.k] === 0) {
    delete leftover[target.k];
  }

  return {
    leftover: leftover,
    amounts: amounts,
    maxNeeded: maxNeeded
  };
}

export function calculateWithNoTarget(opts) {
  let entries = opts.entries;
  let amounts = {};
  let leftover = {};

  let maxLevel = Object.getOwnPropertyNames(opts.entries).reduce((prev, cur) => {
    prev = positiveNumber(prev) || 0;
    cur = positiveNumber(cur) || 0;
    if (cur > prev) {
      return cur;
    } else {
      return prev;
    }
  }, {});

  for (let x = opts.startLevel;
    (x === opts.startLevel || amounts[x] || x <= maxLevel) && x <= opts.endLevel; x++) {
    let next;
    if (opts.optimize === "exact") {
      amounts[x] = (amounts[x] || 0) + (entries[x] || 0);
      next = Math.floor((amounts[x] / 5) * 2);
    } else if (opts.optimize === "5") {
      amounts[x] = (amounts[x] || 0) + (entries[x] || 0);
      next = Math.floor(amounts[x] / 5) * 2;
    } else {
      amounts[x] = (amounts[x] || 0) + (entries[x] || 0);
      next = Math.floor(amounts[x] / 3);
    }

    if (next && x < opts.endLevel) {
      amounts[x + 1] = next;
    }
  }

  return {
    leftover,
    amounts,
  };
}
