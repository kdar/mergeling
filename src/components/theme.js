import {
  useEffect
} from "react";

export function Theme(props) {
  function toggle(props) {
    document.body.classList.toggle("theme-dark", props.dark);
    document.body.classList.toggle("theme-light", !props.dark);
  }

  useEffect(() => {
    toggle(props);

    return function cleanup() {
      document.body.classList.remove("theme-dark", "theme-light");
    };
  });

  return null;
}

Theme.defaultProps = {
  dark: false
};