import React, { useContext } from "react";
import Tippy from "@tippy.js/react";

import { MoreTippyContext } from "./moretippy-context.js";

// A component that allows you to expand the tooltip with
// more information.
export function MoreTippy(props) {
  const context = useContext(MoreTippyContext);

  var { moreContent, toggleExpand, ...remainingProps } = props;

  let text = "less";
  if (!context) {
    moreContent = null;
    text = "more";
  }

  let content = (
    <React.Fragment>
      {props.content}
      {moreContent}
      <div className="card-tooltip-more">
        <span onClick={toggleExpand}>{text}</span>
      </div>
    </React.Fragment>
  );

  return (
    <Tippy {...remainingProps} content={content} interactive={true}>
      {props.children}
    </Tippy>
  );
}
