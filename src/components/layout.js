import React from "react";

import pkg from "../../package.json";
import { Theme } from "./theme.js";
import { Navigation } from "./navigation.js";

export default function Layout(props) {
  return (
    <div className="app">
      <Theme dark={true} />
      <Navigation state={props.state} />
      <div className="container">{props.children}</div>
      <footer>
        Version {pkg.version}
        <br />
        Copyright 2019 <a href="https://github.com/kdar">kdar</a>. All Rights
        Reserved.
        <br />
        <a href="http://mergedragons.com/">Merge Dragons!</a> images are
        Copyright 2019 <a href="http://gram.gs">Gram Games</a>. All Rights
        Reserved.
        <br />
        Suggestions and comments can be made{" "}
        <a href="https://www.reddit.com/r/MergeDragons/comments/bn6p4o/modern_mergedragons_calculator/">
          here
        </a>
        .
      </footer>
    </div>
  );
}
