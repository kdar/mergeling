import React from "react";
import InputNumber from "rc-input-number/es";
import Select from "react-dropdown-select";
import Input from "react-dropdown-select/lib/components/Input.js";
import Tippy from "@tippy.js/react";

import { positiveNumber } from "../calc.js";
import mergeChains from "../merge-chains.js";

const mergeChainSelectOptions = Object.getOwnPropertyNames(mergeChains).map(
  k => ({
    name: mergeChains[k].name,
    value: k
  })
);

const optimizeOptions = [
  {
    name: "Stacks of 5",
    value: "5"
  },
  {
    name: "Stacks of 3",
    value: "3"
  },
  {
    name: "Exact",
    value: "exact"
  }
];

export function Configuration(props) {
  let _lastMergeChain = props.settings.mergeChain;
  let _lastOptimization = props.settings.optimize;

  function values() {
    if (props.settings.mergeChain !== null) {
      return [
        {
          value: props.settings.mergeChain,
          name: mergeChains[props.settings.mergeChain].name
        }
      ];
    }

    return [];
  }

  const onMergeChainChange = e => {
    // Since a merge chain can be null and e[0] can be undefined, we make
    // sure that at least one of them is a truthy value before we continue.
    const atLeastOneIsTruthy = !!_lastMergeChain || !!e[0];
    if (atLeastOneIsTruthy && (!e[0] || _lastMergeChain !== e[0].value)) {
      // Since e[0] can be undefined, check for that and set values
      // accordingly.
      if (!e[0]) {
        props.onSettingChange("mergeChain", null);
        _lastMergeChain = null;
      } else {
        props.onSettingChange("mergeChain", e[0].value);
        _lastMergeChain = e[0].value;
      }
    }
  };

  const onTargetLevelChange = e =>
    props.onSettingChange("target", "k", positiveNumber(e));

  const onTargetCountChange = e =>
    props.onSettingChange("target", "v", positiveNumber(e));

  const onEnableTargetChange = e =>
    props.onSettingChange("enableTarget", !props.settings.enableTarget);

  const onStartLevelChange = e =>
    props.onSettingChange("startLevel", positiveNumber(e));

  const onStackOptimizationChange = e => {
    if (_lastOptimization !== e[0].value) {
      props.onSettingChange("optimize", e[0].value);
      _lastOptimization = e[0].value;
    }
  };

  const contentRenderer = ({ props, state, methods }) => {
    let display = null;
    if (state.values && state.values.length > 0 && state.search === "") {
      const item = state.values[0];
      display = (
        <>
          <div className="mergechain-icon">
            <img
              alt=""
              src={`assets/mergechains/${item.value}/${mergeChains[item.value].preferredIcon}.png`}
            />
          </div>
          <span>{item[props.labelField]}</span>
        </>
      );
    }
    return (
      <>
        {display}
        <Input props={props} methods={methods} state={state} />
      </>
    );
  };

  const itemRenderer = ({ item, itemIndex, props, state, methods }) => (
    <div
      className={`react-dropdown-select-item ${
        methods.isSelected(item) ? "react-dropdown-select-item-selected" : ""
        }`}
      onClick={() => methods.addItem(item)}
    >
      <div className="mergechain-icon">
        <img
          alt=""
          src={`assets/mergechains/${item.value}/${mergeChains[item.value].preferredIcon}.png`}
        />
      </div>
      {item.name}
    </div>
  );

  return (
    <div className="configuration">
      <form>
        <fieldset>
          <div className="form-grid form-grid1">
            <div className="form-grid-1">
              <legend>Merge chain</legend>
            </div>
            <div className="form-grid-2">
              <Select
                options={mergeChainSelectOptions}
                labelField="name"
                searchBy="name"
                sortBy="name"
                valueField="value"
                dropdownGap={0}
                closeOnSelect={true}
                clearable={true}
                values={values()}
                onChange={onMergeChainChange}
                contentRenderer={contentRenderer}
                itemRenderer={itemRenderer}
              />
            </div>
            <div className="form-grid-3">
              <Tippy
                content={
                  "The merge chain you want to follow. This is just a convenience by giving you images, setting the start level, and limiting the max level of target."
                }
              >
                <span className="help" />
              </Tippy>
            </div>
          </div>
        </fieldset>

        <fieldset>
          <div className="form-grid form-grid2">
            <div className="form-grid-1">
              <legend>Target level</legend>
            </div>
            <div className="form-grid-2">
              <legend>Target count</legend>
            </div>
            <div className="form-grid-3">
              <InputNumber
                id="targetLevelField"
                disabled={!props.settings.enableTarget}
                type="number"
                min={0}
                placeholder="Level"
                value={props.settings.target.k}
                onChange={onTargetLevelChange}
              />
            </div>
            <div className="form-grid-4">
              <InputNumber
                id="targetCountField"
                disabled={!props.settings.enableTarget}
                type="number"
                min={0}
                placeholder="Count"
                value={props.settings.target.v}
                onChange={onTargetCountChange}
              />
            </div>
            <div className="form-grid-5">
              <div className="pretty p-default p-curve p-smooth p-toggle">
                <input
                  type="checkbox"
                  checked={props.settings.enableTarget}
                  onChange={onEnableTargetChange}
                />
                <div className="state p-primary-o">
                  <label />
                </div>
              </div>
            </div>
            <div className="form-grid-6">
              <Tippy
                content={
                  <div>
                    This is the level and count which you are targeting. If
                    you want to find out how many previous levels you need to
                    get 2 level 3's, you put 3 and 2 here.
                      <br />
                    <br />
                    If you disable the target by clicking the checkbox, you
                    then enter a mode where it will tell you what all you can
                    make with your current inventory.
                    </div>
                }
              >
                <span className="help" />
              </Tippy>
            </div>
          </div>
        </fieldset>

        {props.settings.mergeChain === null && (
          <fieldset>
            <div className="form-grid form-grid1">
              <div className="form-grid-1">
                <legend>Start at level</legend>
              </div>
              <div className="form-grid-2">
                <InputNumber
                  id="startLevelField"
                  type="number"
                  min={0}
                  placeholder="Start level"
                  value={props.settings.startLevel}
                  onChange={onStartLevelChange}
                />
              </div>
              <div className="form-grid-3">
                <Tippy
                  content={
                    "This is what level to start at. Some merge chains start at 0, 1, or 2."
                  }
                >
                  <span className="help" />
                </Tippy>
              </div>
            </div>
          </fieldset>
        )}

        <fieldset>

          <div className="form-grid form-grid1">
            <div className="form-grid-1">
              <legend>Stack optimization</legend>
            </div>
            <div className="form-grid-2">
              <Select
                options={optimizeOptions}
                values={optimizeOptions.filter(
                  x => x.value === props.settings.optimize
                )}
                closeOnSelect={true}
                labelField="name"
                searchBy="name"
                valueField="value"
                dropdownGap={0}
                clearable={false}
                onChange={onStackOptimizationChange}
              />
            </div>
            <div className="form-grid-3">
              <Tippy
                content={
                  <div>
                    <ul style={{ textAlign: "left" }}>
                      <li>
                        <strong>Stacks of 5</strong>: This will always use
                        stacks of 5. This will cause you to sometimes create
                        more items than we need, but it is the best use of
                        resources. (recommended)
                        </li>
                      <li>
                        <strong>Stacks of 3</strong>: This will always use
                        stacks of 3. This will get you exactly the amount you
                        need, but you will not be optimal in resource usage.
                        </li>
                      <li>
                        <strong>Exact</strong>: This will use a combination of
                        5s and 3s to get exactly the amount you need.
                        </li>
                    </ul>
                  </div>
                }
              >
                <span className="help" />
              </Tippy>
            </div>
          </div>
        </fieldset>

        <fieldset>
          <div className="form-grid form-grid3">
            <div className="form-grid-1">
              <legend>Inventory level</legend>
            </div>
            <div className="form-grid-2">
              <legend>Inventory count</legend>
            </div>
            {props.settings.entries.map((data, index) => (
              <>
                <div className="form-grid-3">
                  <InputNumber
                    type="number"
                    min={0}
                    placeholder="Level"
                    value={data.k === null ? "" : data.k}
                    onChange={e =>
                      props.onEntryChange(index, {
                        k: positiveNumber(e)
                      })
                    }
                  />
                </div>
                <div className="form-grid-4">
                  <InputNumber
                    type="number"
                    min={0}
                    placeholder="Count"
                    value={data.v === null ? "" : data.v}
                    onChange={e =>
                      props.onEntryChange(index, {
                        v: positiveNumber(e)
                      })
                    }
                  />
                </div>
                <div className="form-grid-5">
                  <div
                    className="close"
                    onClick={() => props.onEntryChange(index, null)}
                  />
                </div>

                <div className="form-grid-6">
                  <Tippy
                    content={
                      "This is where you specify how many of each level you already have."
                    }
                  >
                    <span className="help" />
                  </Tippy>
                </div>
              </>
            ))}
          </div>
        </fieldset>
      </form>
    </div >
  );
}
