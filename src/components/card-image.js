import React from "react";

import mergeChains from "../merge-chains.js";
import { MoreTippy } from "./moretippy.js";
import { MoreTippyContext } from "./moretippy-context.js";

// A component that represents a card image with a tooltip.
export function CardImage(props) {
  function content() {
    let v = mergeChains[props.mergeChain].entries[props.index];

    return (
      <div className="card-tooltip">
        <h1>{v.name}</h1>
        <div className="card-tooltip-size">{v["size"] && v.size}</div>
      </div>
    );
  }

  function moreContent() {
    let v = mergeChains[props.mergeChain].entries[props.index];

    return (
      <table className="key-value">
        <tbody>
          {Object.getOwnPropertyNames(v).map(k => {
            if (k === "level" || k === "size" || k === "name" || v[k] === null)
              return null;
            let name = k.replace(/([A-Z])/g, function(str) {
              return " " + str.toLowerCase();
            });

            return (
              <React.Fragment key={k + v[k]}>
                <tr>
                  <td>{name}</td>
                  <td>{v[k]}</td>
                </tr>
              </React.Fragment>
            );
          })}
        </tbody>
      </table>
    );
  }

  return (
    <div className="card-image">
      <MoreTippyContext.Provider value={props.tooltipExpand}>
        <MoreTippy
          content={content()}
          moreContent={moreContent()}
          toggleExpand={props.toggleExpand}
        >
          {props.children}
        </MoreTippy>
      </MoreTippyContext.Provider>
    </div>
  );
}
