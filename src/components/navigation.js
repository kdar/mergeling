import React, { useEffect, useState } from "react";

import { Dropdown } from "./dropdown.js";
import pkg from "../../package.json";
import { encode } from "../state/codecs";

let homepage =
  process.env.NODE_ENV === "development"
    ? `${window.location.protocol}//${window.location.host}`
    : pkg.homepage;

function Menu(props) {
  let ref = React.createRef();

  let [copied, setCopied] = useState(false);

  useEffect(() => {
    ref.current.select();
  });

  return (
    <>
      <div className="menu">
        <h4>Official socials</h4>
        <ul>
          <li>
            <a href="https://discordapp.com/invite/5bBFpY4">Discord</a>
          </li>
          <li>
            <a href="https://www.facebook.com/MergeDragons">Facebook</a>
          </li>
          <li>
            <a href="https://twitter.com/MergeDragons">Twitter</a>
          </li>
        </ul>
        <h4>Fan socials</h4>
        <ul>
          <li>
            <a href="https://www.reddit.com/r/MergeDragons/">Reddit</a>
          </li>
          <li>
            <a href="https://mergedragons.fandom.com/wiki/Merge_Dragons_Wiki">
              Wiki
            </a>
          </li>
        </ul>
      </div>
      <div className="share">
        <h5>Share your current configuration</h5>
        <input
          type="text"
          ref={ref}
          readOnly
          value={homepage + "/#state=" + encode(props.state)}
        />
        <button
          className="button-pop"
          onClick={() => {
            setCopied(true);
            setTimeout(() => setCopied(false), 3000);
            ref.current.focus();
            ref.current.select();
            document.execCommand("copy");
          }}
        >
          {copied ? "Copied!" : "Copy"}
        </button>
      </div>
    </>
  );
}

export function Navigation(props) {
  return (
    <nav className="navigation">
      <section className="container">
        <a className="navigation-title" href={pkg.homepage}>
          <img className="img" alt="" src={"assets/logo.png"} />
          <h1 className="title">Mergeling</h1>
        </a>
        <ul className="navigation-list">
          <li className="navigation-item">
            <Dropdown content={<Menu state={props.state} />}>
              <div className="burger">
                <h2>Menu</h2>
              </div>
            </Dropdown>
          </li>
        </ul>
      </section>
    </nav>
  );
}
