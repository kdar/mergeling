import React from "react";
import Tippy from "@tippy.js/react";

export function Dropdown(props) {
  return (
    <Tippy
      content={props.content}
      interactive={true}
      aria={null}
      placement="bottom"
      distance={7}
      theme="none"
      animation="fade"
      trigger="click"
      arrow={false}
      popperOptions={{
        positionFixed: true
      }}
      onMount={instance => {
        instance.reference.setAttribute("aria-expanded", "true");
      }}
      onHide={instance => {
        instance.reference.setAttribute("aria-expanded", "false");
      }}
    >
      {props.children}
    </Tippy>
  );
}
