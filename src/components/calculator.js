import "pretty-checkbox";
import "react-sweet-progress/lib/style.css";

import React, { useContext, useState, useEffect, useRef } from "react";
import { Progress } from "react-sweet-progress";
import Tippy from "@tippy.js/react";

import { calculateWithNoTarget, calculateWithTarget } from "../calc.js";
import { Configuration } from "./configuration.js";
import { CardImage } from "./card-image.js";
import mergeChains from "../merge-chains.js";
import { defaultEndLevel } from "../state/index.js";
import { StateContext } from "../state/context.js";

function TitleEdit(props) {
  const input = useRef(null);
  useEffect(() => {
    input.current.focus();
    input.current.select();
  }, []);

  const handleKeyDown = e => {
    if (e.key === "Enter") {
      props.onFinish(input.current.value);
    }
  };

  const handleBlur = () => {
    props.onFinish(input.current.value);
  };

  return (
    <input
      ref={input}
      type="text"
      defaultValue={props.title}
      placeholder="Enter the title of this calculator"
      onBlur={handleBlur}
      onKeyDown={handleKeyDown}
    />
  );
}

export function Calculator(props) {
  let lastStartLevel = null;
  const dispatch = useContext(StateContext);
  const state = props.state;
  const [editTitle, setEditTitle] = useState(false);

  function onEntryChange(index, data) {
    var newEntries = JSON.parse(JSON.stringify(state.entries));

    // If data is null, we delete this index.
    if (data === null) {
      newEntries.splice(index, 1);
      // We always want to keep at least one inventory entry
      // because the way the UI works is it automatically adds
      // new entries based on if the current ones are all full.
      if (newEntries.length === 0) {
        newEntries = [{ k: null, v: null }];
      } else if (
        newEntries.length === 1 &&
        (newEntries[0].k !== null || newEntries[0].v !== null)
      ) {
        newEntries.push({ k: null, v: null });
      }
    } else {
      newEntries[index] = Object.assign({}, newEntries[index], data);
      // We add a new entry if we cannot find an empty entry.
      let foundEmpty = false;
      for (let i = 0; i < newEntries.length; i++) {
        if (newEntries[i].k === null && newEntries[i].v === null) {
          foundEmpty = true;
          break;
        }
      }
      if (!foundEmpty) {
        newEntries.push({ k: null, v: null });
      }
    }

    dispatch({ type: "calc-set-entries", data: newEntries });
  }

  // The first of the arguments determines the nested key, the
  // last argument is the value.
  function onSettingChange(/*arguments*/) {
    var s = JSON.parse(JSON.stringify(state));
    let value = arguments[arguments.length - 1];
    let v = s;
    if (arguments.length > 2) {
      v = v[arguments[0]];
      for (let i = 1; i < arguments.length - 2; i++) {
        v = v[arguments[i]];
      }
    }
    if (
      arguments.length === 3 &&
      arguments[0] === "target" &&
      arguments[1] === "k" &&
      s.mergeChain !== null &&
      (value > mergeChains[s.mergeChain].endLevel ||
        value < mergeChains[s.mergeChain].startLevel)
    ) {
      return;
    }
    if (arguments[0] === "mergeChain" && value !== null) {
      if (
        s.target.k > mergeChains[value].endLevel ||
        s.target.k < mergeChains[value].startLevel
      ) {
        s.target.k = mergeChains[value].endLevel;
      }

      lastStartLevel = s.startLevel;
      s.startLevel = mergeChains[value].startLevel;
      s.endLevel = mergeChains[value].endLevel;
    } else if (arguments[0] === "mergeChain" && value === null) {
      s.startLevel = lastStartLevel || 1;
      s.endLevel = defaultEndLevel;
    }
    v[arguments[arguments.length - 2]] = value;

    dispatch({ type: "calc-set", data: s });
  }

  function reset() {
    dispatch({ type: "calc-reset" });
  }

  // Convert the array of entries into an object of entries.
  let settings = {
    ...JSON.parse(JSON.stringify(state)),
    entries: state.entries.reduce((obj, entry) => {
      if (typeof entry.v === "number") {
        obj[entry.k] = entry.v;
      }
      return obj;
    }, {})
  };

  let result;
  if (state.enableTarget) {
    result = calculateWithTarget(settings);
  } else {
    result = calculateWithNoTarget(settings);
  }

  let leftoverNotZeroCount = Object.values(result.leftover).reduce(
    (acc, v) => (v > 0 ? acc + 1 : acc),
    0
  );
  let amountsNotZeroCount = Object.values(result.amounts).reduce(
    (acc, v) => (v > 0 ? acc + 1 : acc),
    0
  );

  const amountCount = Object.getOwnPropertyNames(result.amounts).length;
  let amountsTooltip;

  let progress = undefined;
  if (state.enableTarget) {
    progress =
      (result.maxNeeded[state.startLevel] - result.amounts[state.startLevel]) /
      result.maxNeeded[state.startLevel];
    progress = (progress * 100.0).toFixed(2);
  }

  if (state.enableTarget) {
    amountsTooltip = (
      <div>
        The amounts you need in order to hit your target. This list is
        either-or, meaning you {amountCount > 1 ? "either " : " "}need:
        <div className="tooltip-list">
          {amountCount > 0 &&
            Object.entries(result.amounts)
              .slice(0, 3)
              .map((kv, i) => (
                <ul key={"ul" + i}>
                  <li>
                    {kv[1]} of level {kv[0]}
                    {kv[1] > 1 ? "s" : ""}
                  </li>
                </ul>
              ))
              .reduce((prev, curr, i) => [
                prev,
                <span key={"span" + i}>or</span>,
                curr
              ])}
          {amountCount > 3 ? <span>etc...</span> : null}
        </div>
        A progress bar is displayed to show you how close you are to your
        target.
      </div>
    );
  } else {
    amountsTooltip = (
      <div>The amounts you can create based on your inventory.</div>
    );
  }

  let amountsSection = (
    <div>
      <h4>
        Amounts{"  "}
        <Tippy content={amountsTooltip}>
          <span className="help" />
        </Tippy>
      </h4>

      {progress !== undefined && !isNaN(progress) && (
        <Progress
          percent={progress}
          status={progress === 100 ? "success" : null}
        />
      )}

      <div className="card-container">
        {Object.entries(result.amounts).map((kv, i) => (
          <div key={"amounts-card" + i} className="card">
            <div className="card-title">
              {state.mergeChain !== null && (
                <CardImage
                  index={i}
                  mergeChain={state.mergeChain}
                  tooltipExpand={props.tooltipExpand}
                  toggleExpand={() =>
                    dispatch({ type: "toggle-tooltip-expand" })
                  }
                >
                  <img
                    alt={mergeChains[state.mergeChain].entries[i].name}
                    src={`assets/mergechains/${state.mergeChain}/${i +
                      state.startLevel}.png`}
                  />
                </CardImage>
              )}
              Level {kv[0]}
            </div>
            <div className="card-content">{kv[1]}</div>
          </div>
        ))}
      </div>
    </div>
  );

  let leftoverEntries = Object.entries(result.leftover);
  let leftoverTarget =
    leftoverEntries.length > 0
      ? leftoverEntries.splice(leftoverEntries.length - 1, 1)
      : null;
  let leftoverSection = (
    <div>
      <h4>
        Leftover{"  "}
        <Tippy
          content={
            "How many of each level you'll have leftover after reaching your target."
          }
        >
          <span className="help" />
        </Tippy>
      </h4>

      {leftoverTarget !== null && (
        <div className="card-container">
          <div className="card target">
            <div className="card-title">
              {state.mergeChain !== null && (
                <CardImage
                  index={
                    leftoverTarget[0][0] -
                    mergeChains[state.mergeChain].startLevel
                  }
                  mergeChain={state.mergeChain}
                  tooltipExpand={props.tooltipExpand}
                  toggleExpand={() =>
                    dispatch({ type: "toggle-tooltip-expand" })
                  }
                >
                  <img
                    alt={
                      mergeChains[state.mergeChain].entries[
                        leftoverTarget[0][0] -
                        mergeChains[state.mergeChain].startLevel
                      ].name
                    }
                    src={`assets/mergechains/${state.mergeChain}/${
                      leftoverTarget[0][0]
                      }.png`}
                  />
                </CardImage>
              )}
              Level {leftoverTarget[0][0]}
            </div>
            <div className="card-content">{leftoverTarget[0][1]}</div>
          </div>
        </div>
      )}

      <div className="padder" />

      <div className="card-container">
        {leftoverEntries.map((kv, i) => (
          <div key={"leftover-card" + i} className="card">
            <div className="card-title">Level {kv[0]}</div>
            <div className="card-content">{kv[1]}</div>
          </div>
        ))}
      </div>
    </div>
  );

  return (
    <>
      <div className="panel-title">
        {editTitle ? (
          <TitleEdit
            title={state.title}
            onFinish={t => {
              dispatch({ type: "calc-rename", title: t });
              setEditTitle(false);
            }}
          />
        ) : (
            <h4 onClick={() => setEditTitle(true)}>{state.title}</h4>
          )}
      </div>
      <div className="panel-content">
        <Configuration
          settings={state}
          onEntryChange={(a, b) => onEntryChange(a, b)}
          onSettingChange={(...args) => onSettingChange(...args)}
        />
        <button className="button button-pop-outline" onClick={() => reset()}>
          Reset configuration
        </button>
        <hr />
        {amountsSection}
        <div className="padder" />
        {leftoverNotZeroCount > 0 && leftoverSection}
        {amountsNotZeroCount === 0 && leftoverNotZeroCount === 0 && (
          <div>No results!</div>
        )}
      </div>
    </>
  );
}
