FROM node:11.14.0-alpine
COPY . /app

WORKDIR /app
ENV CI=true

RUN rm -rf node_modules
RUN npm install
RUN npm run build-obfuscate
RUN mv public _public && mv build public
